# Build UCSC Chain file for LiftOver


## Description

Workflow to generate [UCSC chain
files](https://genome.ucsc.edu/goldenpath/help/chain.html) between two genomes.
These files can be used by tools such as [UCSC's LiftOver tool]() or
[CrossMap](http://crossmap.sourceforge.net/) to convert coordinates of genomic
features from one version of an assembly to another.

The workflow was created with help from "[How to create a chain/liftover
file](http://genomewiki.ucsc.edu/index.php/LiftOver_Howto)".

**Note**: This workflow is best suited for conversion between assemblies of the
same organism.

The workflow is written using [Snakemake](https://snakemake.readthedocs.io/).

Dependencies are installed using [Bioconda](https://bioconda.github.io/) where
possible.


## Input files

The input to this workflow is two fasta files, one for the old assembly and one
for the new assembly. Ideally, the new assembly will have been
[repeat masked](http://www.repeatmasker.org/).


## Alternatives

Alternative alignment options:

* [chainCleaner](https://academic.oup.com/bioinformatics/article/33/11/1596/2929344)
  - improves the specificity in genome alignments by accurately detecting and
    removing local alignments that obscure the evolutionary history of genomic
    rearrangements

* [Mugsy](https://academic.oup.com/bioinformatics/article/27/3/334/320837) -
  fast multiple alignment of closely related whole genomes

* [LAGAN](http://lagan.stanford.edu/lagan_web/citing.shtml) - Whole genome
  aligner


Alternative approaches include:

* [Ensembl's Assembly
  Converter](http://www.ensembl.org/Homo_sapiens/Tools/AssemblyConverter) - Web
  interface as well as API

* [NCBI's Remap](https://www.ncbi.nlm.nih.gov/genome/tools/remap/docs/whatis) -
  Web interface and API

* [Flo](https://github.com/wurmlab/flo) - A similar workflow based on UCSC's
  tools.

See [Biostars](https://www.biostars.org/p/65558/) for a discussion.


## Setup environment and run workflow

1.  Clone workflow into working directory

        git clone https://bitbucket.org/princeton_genomics/liftover_workflow.git path/to/workdir
        cd path/to/workdir

2.  Place input fasta files for both old and new genome into `data` subdirectory

        mkdir data
        cp /path/to/old.fasta data/old.fasta
        cp /path/to/new.fasta data/new.fasta

3.  Edit config as needed


        cp config.yaml.sample config.yaml
        nano config.yaml

4.  Install dependencies into isolated environment

        conda env create -n liftover_workflow --file environment.yaml

5.  Activate environment

        source activate liftover_workflow

6.  Execute workflow

        snakemake -s splitByName.Snakefile
        snakemake

## Running workflow on cetus (`gen-comp1`).

    ./run_snakemake_cetus.sh
    liftOver OLD.gff OLD_to_NEW.over.chain.gz OLD_to_NEW.gff

