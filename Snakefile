# vi:syntax=snakemake
# Snakemake script to generate ribosomal occupancy counts using plastid

# Required inputs:
#   old_genome.fa
#   new_genome.fa
#
# Software dependencies
#   environment.yaml is list of conda packages

configfile: "config.yaml"
INPUT_DIR=config['input_dir'] + "/"
OUTPUT_DIR=config['output_dir'] + "/"
OLD_GENOME=config['old_genome']
NEW_GENOME=config['new_genome']

SCAFFOLDS, = glob_wildcards(OUTPUT_DIR + NEW_GENOME + "_split_by_name/{scaffold}.fa")

rule all:
    input: OUTPUT_DIR + OLD_GENOME + "_to_" + NEW_GENOME + ".over.chain.gz"

#rule filter_by_length:
#    input: "data/{file}.fa"
#    output: "output/{file}_min{length,\d+}.fa"
#    shell:
#        r'''
#        seqtk seq -L {wildcards.length} "{input}" > "{output}"
#        '''

#rule fasplit_byname:
#    input: INPUT_DIR + NEW_GENOME + ".fa"
#    params:
#        outdir=OUTPUT_DIR + NEW_GENOME + "_split_by_name/"
#    output: dynamic(OUTPUT_DIR + NEW_GENOME + "_split_by_name/{scaffold}.fa")
#    shell:
#        r'''
#        faSplit byname {input:q} {params.outdir:q}
#        '''

rule fasplit:
    input: OUTPUT_DIR + NEW_GENOME + "_split_by_name/{scaffold}.fa"
    params:
        prefix=OUTPUT_DIR + NEW_GENOME + "_split_by_size/{scaffold}"
    output: split_fasta=OUTPUT_DIR + NEW_GENOME + "_split_by_size/{scaffold}.fa",
            lift_file=OUTPUT_DIR + NEW_GENOME + "_lift/{scaffold}.lft"
    shell:
        r'''
        faSplit size -lift="{output.lift_file}" "{input}" -oneFile 5000 "{params.prefix}"
        '''

rule twobit:
    input: "{file}.fa"
    output: "{file}.2bit"
    shell: 'faToTwoBit "{input}" "{output}"'

rule twobitinfo:
    input: "{file}.2bit"
    output: "{file}.chrom.sizes"
    shell: 'twoBitInfo "{input}" "{output}"'

rule blat:
    input: scaffold=OUTPUT_DIR + NEW_GENOME + "_split_by_size/{scaffold}.fa",
           old_genome_2bit=INPUT_DIR + OLD_GENOME + ".2bit"
    output: OUTPUT_DIR + NEW_GENOME + "_psl/{scaffold}_to_" + OLD_GENOME + ".psl"
    shell: 'blat "{input.old_genome_2bit}" "{input.scaffold}" -tileSize=12 -fastMap -minIdentity=98 -minScore=100 "{output}"'

rule liftUp:
    input: psl=OUTPUT_DIR + NEW_GENOME + "_psl/{scaffold}_to_" + OLD_GENOME + ".psl",
           lift_file=OUTPUT_DIR + NEW_GENOME + "_lift/{scaffold}.lft"
    output: OUTPUT_DIR + NEW_GENOME + "_psl_lifted/{scaffold}_to_" + OLD_GENOME + ".psl",
    shell:
        r'''
        liftUp -pslQ "{output}" "{input.lift_file}" warn "{input.psl}"
        '''

rule axtChain:
    input: psl=OUTPUT_DIR + NEW_GENOME + "_psl_lifted/{scaffold}_to_" + OLD_GENOME + ".psl",
           old_genome_2bit=INPUT_DIR + OLD_GENOME + ".2bit",
           new_genome_2bit=INPUT_DIR + NEW_GENOME + ".2bit"
    output: OUTPUT_DIR + OLD_GENOME + "_to_" + NEW_GENOME + "_chainRaw/{scaffold}.chain"
    shell:
        r'''
        axtChain -linearGap=medium -psl "{input.psl}" "{input.old_genome_2bit}" "{input.new_genome_2bit}" "{output}"
        '''

# TODO Seems to be a bug in snakemake: 
# https://groups.google.com/forum/#!topic/snakemake/i3JiUeS1cFw
rule chainRawList:
#    input: dynamic(OUTPUT_DIR + OLD_GENOME + "_to_" + NEW_GENOME + "_chainRaw/{scaffold}.chain")
    input: expand(OUTPUT_DIR + OLD_GENOME + "_to_" + NEW_GENOME + "_chainRaw/{scaffold}.chain", scaffold=SCAFFOLDS)
    output: OUTPUT_DIR + OLD_GENOME + "_to_" + NEW_GENOME + "_chainRawList.txt"
    run:
        with open(output[0], 'w') as out:
            for f in input:
                out.write("{}\n".format(f))
#    params:
#        inputdir=OUTPUT_DIR + OLD_GENOME + "_to_" + NEW_GENOME + "_chainRaw/"
#    shell: "ls -1 {params.inputdir:q}/*.chain > {output:q}" 

rule chainMerge:
    input: OUTPUT_DIR + OLD_GENOME + "_to_" + NEW_GENOME + "_chainRawList.txt"
    output: OUTPUT_DIR + OLD_GENOME + "_to_" + NEW_GENOME + "_chainMerge"
    params:
        tmpdir=OUTPUT_DIR + "chainRawSortTmp"
    shell:
        r'''
        mkdir -p {params.tmpdir:q}
        chainMergeSort -inputList="{input}" -tempDir={params.tmpdir:q} | chainSplit {output} stdin -lump=50
        '''

rule chainSort:
    input: OUTPUT_DIR + OLD_GENOME + "_to_" + NEW_GENOME + "_chainMerge"
    output: all=OUTPUT_DIR + OLD_GENOME + "_to_" + NEW_GENOME +"_all.chain",
            sorted=OUTPUT_DIR + OLD_GENOME + "_to_" + NEW_GENOME +"_all.sorted.chain"
    shell:
        r'''
        cat "{input}"/*.chain > "{output.all}"
        chainSort "{output.all}" "{output.sorted}"
        '''

rule chainNet:
    input: chain=OUTPUT_DIR + OLD_GENOME + "_to_" + NEW_GENOME + "_all.sorted.chain",
           old_chrom_sizes=INPUT_DIR + OLD_GENOME + ".chrom.sizes",
           new_chrom_sizes=INPUT_DIR + NEW_GENOME + ".chrom.sizes"
    output: OUTPUT_DIR + OLD_GENOME + "_to_" + NEW_GENOME + ".net"
    shell:
        r'''
        chainNet "{input.chain}" "{input.old_chrom_sizes}" "{input.new_chrom_sizes}" "{output}" /dev/null
        '''

rule netChainSubset:
    input: net=OUTPUT_DIR + OLD_GENOME + "_to_" + NEW_GENOME + ".net",
           chain=OUTPUT_DIR + OLD_GENOME + "_to_" + NEW_GENOME + "_all.sorted.chain",
    output: OUTPUT_DIR + OLD_GENOME + "_to_" + NEW_GENOME + ".over.chain"
    shell:
        r'''
        netChainSubset "{input.net}" "{input.chain}" "{output}"
        '''

rule gzip:
    input: "{file}"
    output: "{file}.gz"
    shell: "gzip {input:q}"
