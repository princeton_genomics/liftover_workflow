#!/bin/bash

# TODO Check for existence of config.yaml and cluster.yaml

# TODO Optional parameters to specify config and cluster files

snakemake --cluster-config 'cetus_cluster.yaml' \
          --drmaa " --cpus-per-task={cluster.n} --mem={cluster.memory} --qos={cluster.qos}" \
          --use-conda -rp -w 60 -j 1000 -s "splitByName.Snakefile" "$@"

snakemake --cluster-config 'cetus_cluster.yaml' \
          --drmaa " --cpus-per-task={cluster.n} --mem={cluster.memory} --qos={cluster.qos}" \
          --use-conda -rp -w 60 -j 1000 "$@"

