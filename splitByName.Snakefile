# Split input chromosome by name
# Separate workflow due to Snakemake bug with dynamic files and NFS on cetus
# TODO This seems to be a bug in snakemake: 
# https://groups.google.com/forum/#!topic/snakemake/i3JiUeS1cFw
#
configfile: "config.yaml"
INPUT_DIR=config['input_dir'] + "/"
OUTPUT_DIR=config['output_dir'] + "/"
OLD_GENOME=config['old_genome']
NEW_GENOME=config['new_genome']

rule all:
    #input: dynamic(OUTPUT_DIR + NEW_GENOME + "_split_by_name/{scaffold}.fa")
    input: OUTPUT_DIR + NEW_GENOME + "_split_by_name.done"
    

rule fasplit_byname:
    input: INPUT_DIR + NEW_GENOME + ".fa"
    params:
        outdir=OUTPUT_DIR + NEW_GENOME + "_split_by_name/"
    #output: dynamic(OUTPUT_DIR + NEW_GENOME + "_split_by_name/{scaffold}.fa")
    output: OUTPUT_DIR + NEW_GENOME + "_split_by_name.done"
    shell:
        r'''
        faSplit byname {input:q} {params.outdir:q} && touch {output:q}
        '''


# vi:syntax=snakemake
